const tabs = document.querySelectorAll('.tabs-title');
const contents = document.querySelectorAll('.tabs-content > li');

// Перехід між вкладками
tabs.forEach(tab => {
  tab.addEventListener('click', () => {
    // Виключаєм попередню активну вкладку та контент
    tabs.forEach(tab => tab.classList.remove('active'));
    contents.forEach(content => content.style.display = 'none');
    
    // Активуємо потрібну вкладку та відображаємо заданий контент
    tab.classList.add('active');
    const content = document.querySelector(`.tabs-content li[data-tab="${tab.dataset.tab}"]`);
    content.style.display = 'block';
  });
});